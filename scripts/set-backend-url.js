var fs = require('fs')
var path = require('path')

let backendUrl = 'https://app.aula.de/api/'

console.info('Optionally pass a single <url> (e.g. dev.aula.de) command line argument')

if (process.argv.length === 3) {
    backendUrl = (process.argv[2])
}
try {
    const file = path.join(__dirname, '..', 'static/js/config.js')
    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            return console.log(err)
        }
        var result = data.replace(/baseUrl = 'http:\/\/localhost:8082\/api\/'/g, `baseUrl = '${backendUrl}'`)

        fs.writeFile(file, result, 'utf8', function (err) {
            if (err) return console.log(err)

            console.info('using app backend ', backendUrl)
        })
    })
} catch (err) {
    console.error(err)
}