# Roadmap

Short term - What we are working on now 🎉

* push notifications
* bulk operation on the admin interface
* filtering by categories
* mark ideas as winner
* move topics between rooms
* automated bug reporting
* improved delegation ux

Medium term - what we’re working on next! 🎨

* customizable picture on rooms and topics
* custom fields for ideas creations
* oauth authentication
* reset password by email


Longer term items - working on this soon! ⏩

* custom themes for the interface
* Archive
* search function for delegation
* hide delegation function
