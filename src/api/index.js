import service from './service'
import * as topic from './topic'
import * as ideaSpace from './ideaSpace'
import school from './school'
import user from './user'
import comment from './comment'
import category from './category'
import firebase from './firebase'
import idea from './idea'

export default {
  category,
  comment,
  firebase,
  idea,
  ideaSpace,
  school,
  service,
  topic,
  user
}
