FROM node:14

WORKDIR aula/
VOLUME /aula/www/
COPY package*.json ./
RUN npm install
COPY . .
ENV "ENV" "production"
CMD ["npm", "run", "build"]
